window.CellsBehaviors = window.CellsBehaviors || {};
CellsBehaviors.DTesterAppBehavior = (superClass) => class extends superClass {

  getUrlParams(path, params) {
    let urlParams = Object.keys(params).map(function(k) {
      return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
    }).join('&');
    return `${path}?${urlParams}`;
  }

  parseUrlParams(url) {
    try {
      let search = url.substring(url.indexOf('?') + 1);
      return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
    } catch (error) {
      console.warn(error);
    }
    return {};
  }

  dispatch(name, detail) {
    const val = typeof detail === 'undefined' ? null : detail;
    this.dispatchEvent(new CustomEvent(name, {
      composed: true,
      bubbles: true,
      detail: val
    }));
  }

  extract(data, keys, value) {
    let ret;
    if (!this.isEmpty(data) && !this.isEmpty(keys)) {
      let split = keys.split('.');
      ret = data[split.shift()];
      while (ret && split.length) {
        ret = ret[split.shift()];
      }
    }
    return this.isEmpty(ret) && value !== null ? value : ret;
  }

  isEmpty(evaluate) {
    return this._isEmpty(evaluate);
  }

  isNotEmpty(evaluate) {
    return !this._isEmpty(evaluate);
  }

  _isEmpty(evaluate) {
    switch (typeof(evaluate)) {
      case 'object':
        return evaluate === null || Object.keys(evaluate).length === 0;
      case 'string':
        return evaluate === '';
      case 'undefined':
        return true;
      default:
        return false;
    }
  }

  /**
      * method that adapts the String format to HTML
      * @method validateForm
      * @param {String} stringHtml
      */
  adaptStringHtml(stringHtml) {
    var t = document.createElement('template');
    t.innerHTML = stringHtml;
    return t.content.cloneNode(true);
  }

};