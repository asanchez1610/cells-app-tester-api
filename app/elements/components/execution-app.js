import { LitElement, html, css } from 'lit-element';
import '@cells-components/cells-icon';
import '@bbva-web-components/bbva-form-field';
import '../utils/behavior.js';
import stylesButtons from '../styles/buttons-styles.js';
import '@cells-components/cells-icon';

const utilBehavior = CellsBehaviors.DTesterAppBehavior;
class ExecutionApp extends utilBehavior(LitElement) {
  static get styles() {
    return [
      stylesButtons,
      css`
      :host {
        box-sizing: border-box;
        margin:0;
        padding:0;
        position:relative;
      }

      :host([hidden]), [hidden] {
        display: none !important;
      }

      header {
        width: 100%;
        height:48px;
        display:flex;
        align-items:center;
        background-color:#f4f4f4;
      }

      .container-execution {
        width:100%;
        height: calc(100vh - 100px);
        background-color: #2B2C2A;
        color: #fff;
      }

      .container-execution section {
        height: calc(100vh - 100px);
      }

      .title-info {
          font-size: 15px;
          padding-left: 20px;
          width: calc(100% - 260px);
      }

      .title-info span {
        color: #1973B8;
      }

      .btn-ejecutar {
        width:100px;
        height: 48px !important;
        outline: none;
      }

      .btn-cancelar {
        width:100px;
        height: 48px !important;
        outline: none;
      }

      a.btn-report {
        width: 60px;
        outline: none;
        line-height: 26px;
        top: 0;
        position: relative;
      }

      .content-executions-items {
        height: calc(100vh - 100px);
        overflow-x: hidden;
        overflow-y: auto;
        padding-top:5px;
      }

      .executions-item {
        background-color: #EAF9FA;
        padding: 10px 15px;
        margin:15px 20px;
        color: #000;
        font-size:15px;
        border-bottom: 3px solid #006C6C;
        position:relative;
        display: flex;
        align-items:center;
      }

      .executions-item-error {
        border-bottom: 3px solid #B92A45;
        background-color: #FDE7D8;
      }

      .executions-item-tittle {
        width: calc(100% - 480px);
      }

      .executions-item-consumer-id {
        width:450px;
        text-align:right;
        font-size: 13px;
        padding-right: 5px;
        position: relative;
        top: 2px;
      }
      .executions-item-icon {
        align-items:center;
        justify-content: center;
        display:flex;
        width:30px;
        text-align:right;
      }

      .spinner,.spinner:after{
          display: block;
          width: 13px;
          height: 13px;
          border-radius: 50%;
      }
      .spinner {
          background-color: transparent;
          border-top: 2px solid rgb(66,139,202);
          border-right: 2px solid rgb(66,139,202);
          border-bottom: 2px solid rgb(66,139,202);
          border-left: 2px solid rgba(66,139,202,.2);
          animation-iteration-count: infinite;
          animation-timing-function: linear;
          animation-duration: .8s;
          animation-name: spinner-loading
      }
      @keyframes spinner-loading{
        0% {
            transform: rotate(0deg)
        } to {
            transform: rotate(1turn)
        }
      }

      .icon-result {
        width: 20px;
        height: 20px;
      }

      .icon-success {
        color: #48AE64;
      }

      .icon-error {
        color: #CB353A;
      }

      .hide {
        display:none !important;
      }

    `];
  }

  static get properties() {
    return {
      executions: Array,
      position: Number,
      total: Number,
      showResultCount: Boolean,
      errors: Number,
      success: Number,
      reportItems: Array
    };
  }

  constructor() {
    super();
    this.execution = {};
    this.executions = [];
    this.reportItems = [];
    this.position = 0;
    this.total = 0;
    this.showResultCount = false;
    this.success = 0;
    this.errors = 0;
  }

  tplIconPending(item) {
    if (item.pending) {
      return html`<span class="spinner"></span>`;
    }
    return html`<cells-icon class = "icon-result ${item.success ? 'icon-success' : 'icon-error'}" icon = "coronita:${item.success ? 'correct' : 'error'}" ></cells-icon>`;
  }

  tplItem(item) {
    return html`
      <div class = "executions-item${!item.pending && !item.success ? ' executions-item-error' : ''}">
        <div class = "executions-item-tittle">${item.name}</div>
        <div class = "executions-item-consumer-id" title = "consumerrequestid">${item.consumerId}</div>
        <div class = "executions-item-icon" >${this.tplIconPending(item)}</div>
      </div>
    `;
  }

  getTplReport(rowsStr) {
    return `
            <style>
            *{
            margin:0;
            }
            table {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
            font-family: sans-serif;
            font-size:12px;
            }

            th {
            text-align:left;
            padding:10px;
            background-color:#072146;
            color:#fff;
            }

            td {
            padding:10px;
            background-color:#fff;
            color:#121212;
            border-bottom:1px solid #e1e1e1;
            }

            </style>

            <table>
              <tr>
                <th width = "20%">SMC/Servicio</th><th width = "20%">Consumer ID</th><th width = "7%">Status</th><th width = "53%">Response</th>
              </tr>
              ${rowsStr}
            </table>
    `;
  }

  enableButtons(enable) {
    if (enable) {
      this.shadowRoot.querySelector('#btn-ejecutar').removeAttribute('disabled');
      this.shadowRoot.querySelector('#btn-cancelar').removeAttribute('disabled');
      this.shadowRoot.querySelector('#btn-ejecutar').classList.remove('disabled');
      this.shadowRoot.querySelector('#btn-cancelar').classList.remove('disabled');
    } else {
      this.shadowRoot.querySelector('#btn-ejecutar').setAttribute('disabled', 'disabled');
      this.shadowRoot.querySelector('#btn-cancelar').setAttribute('disabled', 'disabled');
      this.shadowRoot.querySelector('#btn-ejecutar').classList.add('disabled');
      this.shadowRoot.querySelector('#btn-cancelar').classList.add('disabled');
    }
  }

  async initRun(index, item) {
    if (index === 0) {
      this.executions = [];
      this.success = 0;
      this.errors = 0;
      this.showResultCount = false;
      this.reportItems = [];
      this.enableButtons(false);
    }
    this.position = index;
    this.executions.push({
      name: item.name,
      pending: item.pending
    });
    await this.requestUpdate();
  }

  addReport(item) {
    let headers = {};
    let consumerId = 'undefined';
    if (item && item.request && item.request.headers) {
      headers = item.request.headers;
      consumerId = headers.get('consumerrequestid');
    }
    let strResponse = '';
    try {
      strResponse = JSON.stringify(item.response);
    } catch (error) {
      console.log('Error parse object response report', error);
    }
    this.reportItems.push(
      {
        service: item.name,
        smc: item && item.itemRequest && item.itemRequest.smc ? item.itemRequest.smc : '',
        consumerId: consumerId,
        status: item.request.status,
        response: strResponse
      }
    );
    this.requestUpdate();
  }

  async nextItem(position, item) {
    this.position = position;
    let headers = {};
    let consumerId = 'undefined';
    if (item && item.request && item.request.headers) {
      headers = item.request.headers;
      consumerId = headers.get('consumerrequestid');
    }
    this.addReport(item);
    if (item.success) {
      this.success++;
    } else {
      this.errors++;
    }
    let temp = [ ...this.executions ];
    this.executions = temp.map(itemMap => {
      if (itemMap.name === item.name) {
        return {
          name: item.name,
          pending: item.pending,
          success: item.success,
          consumerId: consumerId
        };
      }
      return itemMap;
    });
    await this.requestUpdate();
    this.dispatch('on-execution', position);
  }

  cancelar() {
    this.position = 0;
    this.success = 0;
    this.errors = 0;
    this.showResultCount = false;
    this.executions = [];
    this.reportItems = [];
    this.dispatch('on-cancel-execution', {});
  }

  reportBuild() {
    this.shadowRoot.querySelector('#btn-report').classList.remove('disabled');
    let strRows = '';
    this.reportItems.forEach(item => {
      strRows += `<tr>
                    <td>${item.smc ? item.smc + '/' : '' }${item.service}</td><td>${item.consumerId}</td><td>${item.status}</td><td>${item.response}</td>
                  </tr>`;
    });
    let htmlReport = this.getTplReport(strRows);
    let blob = new Blob([ htmlReport ], { type: 'text/html' });
    let url = window.URL.createObjectURL(blob);
    let a = this.shadowRoot.querySelector('#btn-report');
    a.href = url;
    a.download = 'report-requests.html';
  }

  disabledReport() {
    let a = this.shadowRoot.querySelector('#btn-report');
    a.removeAttribute('href');
    a.removeAttribute('download');
    a.classList.add('disabled');
  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
      <header>
        <div class = "title-info" >
          Peticiones para ejecutar: <span>${this.position}/${this.total}</span> <span ?hidden = "${!this.showResultCount}" style = "color:#48AE64;">- Correctos: ${this.success}</span> <span style = "color:#CB353A;" ?hidden = "${!this.showResultCount}">- Errores: ${this.errors}</span>
        </div>
        <button @click = ${() => this.dispatch('on-execution', 0)} id = "btn-ejecutar"  class = "btn-ejecutar button shadow-blue ">Ejecutar</button>
        <a id = "btn-report" class = "btn-report button blue disabled" >Reporte</a>
        <button @click = ${() => this.cancelar()} id = "btn-cancelar"  class = "btn-cancelar button red ">Cerrar</button>

      </header>
      <main class = "container-execution">
        <section class = "content-executions-items">
          ${(this.executions || []).map((item) => html`${this.tplItem(item)}`)}
        </section>
      </main>
    `;
  }
}
customElements.define('execution-app', ExecutionApp);