import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import styles from '../../elements/styles/main-styles.js';
import stylesButtons from '../../elements/styles/buttons-styles.js';
import '@quotation-components/cells-modal-pfco';
import '../../elements/components/menu-app.js';
import '../../elements/components/request-app.js';
import '../../elements/components/form-request-app.js';
import '../../elements/components/form-config-app.js';
import '../../elements/components/execution-app.js';
import { repository } from '../../elements/database/repository.js';
import { sendRequest } from '../../elements/utils/send-request.js';
import '@quotation-components/cells-toast-pfco';
import '@quotation-components/cells-access-control-pfco';
import '@quotation-components/cells-spinner-global-pfco';
import '@bbva-web-components/bbva-help-modal';

class MainPage extends CellsPage {
  static get is() {
    return 'main-page';
  }

  constructor() {
    super();
    this.configurations = [];
    this.requests = [];
    this.addListers();
  }

  static get properties() {
    return {
      configurations: Array,
      requests: Array
    };
  }

  async addListers() {
    await this.updateComplete;
    window.addEventListener('db-on-ready', () => this.initData());
    window.addEventListener('list-request-complete', ({ detail }) => this.loadRequest(detail));
    window.addEventListener('list-config-complete', ({ detail }) => this.loadConfig(detail));
    window.addEventListener('autentication-pfc-complete', () => this.shadowRoot.querySelector('cells-access-control-pfco').maskVisible = false);
    window.addEventListener('on-loading-mask', ({ detail }) => this.shadowRoot.querySelector('cells-spinner-global-pfco').activeLoading = detail);
    window.addEventListener('on-error-request-save', () => this.shadowRoot.querySelector('#toast').showToast({ type: 'error', message: 'Error! La petición ya existe ó los datos enviados son incorrectos.' }));
    window.addEventListener('on-success-request-save', () => this.shadowRoot.querySelector('#toast').showToast({ type: 'success', message: 'Las modificaciones han sido aplicadas.' }));
    window.addEventListener('autentication-unsuccess', () => {
      this.shadowRoot.querySelector('bbva-help-modal').open();
      this.shadowRoot.querySelector('cells-access-control-pfco').maskVisible = false;
    });
  }

  get repository() {
    return repository;
  }

  dispatch(name, data) {
    const customEvent = new CustomEvent(name, {
      detail: data,
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(customEvent);
  }

  openForm(type, data) {
    if (type === 'edit') {
      this.shadowRoot.querySelector('form-request-app').setDataRequest(data);
    }
    this.shadowRoot.querySelector('#modalCreate').open();
  }

  validateCreate(data) {
    if (!data.id) {
      let temp = this.requests.filter(item => item.name.toUpperCase() === data.name.toUpperCase());
      if (temp && temp.length > 0) {
        this.shadowRoot.querySelector('#toast').showToast({type: 'error', message: `La petición ${data.name} ya existe.` });
        return false;
      }
    }
    return true;
  }

  saveRequest(data, indicator) {
    if (!this.validateCreate(data)) {
      return;
    }
    this.repository.saveRequest(data, indicator);
    this.shadowRoot.querySelector('request-app').clear();
    this.shadowRoot.querySelector('menu-app').clearSelectedItems();
  }

  async saveConfig(data) {
    this.shadowRoot.querySelector('cells-spinner-global-pfco').activeLoading = true;
    data = data || [];
    data.forEach(element => {
      this.repository.saveConfig(element);
    });
    let urlGt = this.getConfig(data, 'grantingTicket');
    let urlNautilius = this.getConfig(data, 'urlLoginNautilius');
    if (urlGt) {
      this.shadowRoot.querySelector('#dm-control-access').setAttribute('urlGrantingTicket', urlGt);
      this.shadowRoot.querySelector('#dm-control-access').setAttribute('urlLoginNautilius', urlNautilius);
      this.shadowRoot.querySelector('#dm-control-access').urlGrantingTicket = urlGt;
      this.shadowRoot.querySelector('#dm-control-access').urlLoginNautilius = urlNautilius;
      await this.shadowRoot.querySelector('#dm-control-access').runEmployeeAuthentication();
    }
    this.shadowRoot.querySelector('cells-spinner-global-pfco').activeLoading = false;
  }

  initData() {
    this.repository.listRequest();
    this.repository.listConfigurations();
  }

  loadRequest(data) {
    this.requests = data;
    this.shadowRoot.querySelector('menu-app').items = data;
    this.shadowRoot.querySelector('request-app').clear();
    this.shadowRoot.querySelector('menu-app').clearSelectedItems();
  }

  async loadConfig(data) {
    console.log('loadConfig', data);
    this.configurations = data;
    this.shadowRoot.querySelector('form-config-app').setDataConfiguration(data);
    let urlGt = this.getConfig(data, 'grantingTicket');
    let urlNautilius = this.getConfig(data, 'urlLoginNautilius');
    if (urlGt) {
      this.shadowRoot.querySelector('#dm-control-access').setAttribute('urlGrantingTicket', urlGt);
      this.shadowRoot.querySelector('#dm-control-access').setAttribute('urlLoginNautilius', urlNautilius);
      this.shadowRoot.querySelector('#dm-control-access').urlGrantingTicket = urlGt;
      this.shadowRoot.querySelector('#dm-control-access').urlLoginNautilius = urlNautilius;
    }
    await this.shadowRoot.querySelector('#dm-control-access').runEmployeeAuthentication();
  }

  onPageEnter() {
    if (this.repository.db) {
      this.repository.listRequest();
    }
  }

  onPageLeave() { }

  static get styles() {
    return [
      styles,
      stylesButtons
    ];
  }

  selectedItem(item) {
    this.shadowRoot.querySelector('request-app').setRequest(item);
    this.shadowRoot.querySelector('request-app').hideVieTextResponse();
  }

  deleteRequest({ data }) {
    this.repository.deleteRequest(data.id);
    this.shadowRoot.querySelector('#modalCreate').close();
  }

  copyTsec() {
    this.shadowRoot.querySelector('.textarea-tsec').value = window.sessionStorage.getItem('tsec');
    this.shadowRoot.querySelector('#modalTsec').open();
  }

  getConfig(configs, key) {
    if (configs && configs.length > 0) {
      let temp = configs.filter((confg) => confg.name === key);
      if (temp[0]) {
        return temp[0].value;
      }
    }
    return null;
  }

  validateVariables(input) {
    let serverBase = this.getConfig(this.configurations, 'serverBase');
    let idRequest = this.getConfig(this.configurations, 'idRequest');
    let idQuotation = this.getConfig(this.configurations, 'idQuotation');
    let contactID = this.getConfig(this.configurations, 'contactID');
    let customerId = this.getConfig(this.configurations, 'customerId');
    let documentType = this.getConfig(this.configurations, 'documentType');
    let documentNumber = this.getConfig(this.configurations, 'documentNumber');
    if (serverBase) {
      input = input.replace(/{{serverBase}}/, serverBase);
    }
    if (idRequest) {
      input = input.replace(/{{idRequest}}/, idRequest);
    }
    if (idQuotation) {
      input = input.replace(/{{idQuotation}}/, idQuotation);
    }
    if (contactID) {
      input = input.replace(/{{contactID}}/, contactID);
    }
    if (customerId) {
      input = input.replace(/{{customerId}}/, customerId);
    }
    if (documentType) {
      input = input.replace(/{{documentType}}/, documentType);
    }
    if (documentNumber) {
      input = input.replace(/{{documentNumber}}/, documentNumber);
    }
    return input;
  }

  validateJsonVariables(objJson) {
    try {
      let strObjJson = JSON.stringify(objJson);
      strObjJson = this.validateVariables(strObjJson);
      return JSON.parse(strObjJson);
    } catch (error) {
      return objJson;
    }
  }

  async sendRequest(data) {
    let url = this.validateVariables(data.url);
    let begin = Date.now();
    let { response, request } = await sendRequest.run(url, data.method, this.validateJsonVariables(data.headers), this.validateJsonVariables(data.body));
    let end = Date.now();
    this.shadowRoot.querySelector('request-app').showResponse(response, request, (end - begin));
  }

  export() {
    let objExport = {
      requests: this.requests,
      configurations: this.configurations
    };
    let strRequest = JSON.stringify(objExport, null, '  ');
    this.shadowRoot.querySelector('#text-export').value = strRequest;
    let blob = new Blob([ strRequest ], { type: 'octet/stream' });
    let url = window.URL.createObjectURL(blob);
    let a = this.shadowRoot.querySelector('#btn-exportar');
    a.href = url;
    a.download = 'export-requests.txt';
    this.shadowRoot.querySelector('#modalExport').open();
  }

  importar() {
    this.shadowRoot.querySelector('#modalImport').open();
  }

  enableButtonImport(enable) {
    if (enable) {
      this.shadowRoot.querySelector('#btn-importar').removeAttribute('disabled');
      this.shadowRoot.querySelector('#btn-importar').classList.remove('disabled');
    } else {
      this.shadowRoot.querySelector('#btn-importar').setAttribute('disabled', 'disabled');
      this.shadowRoot.querySelector('#btn-importar').classList.add('disabled');
    }
  }

  validateInputImport() {
    let strJson = this.shadowRoot.querySelector('#text-import').value;
    if (!strJson) {
      this.enableButtonImport(false);
      this.shadowRoot.querySelector('.confirm-import').classList.add('hide');
      return null;
    }
    try {
      let temObjJson;
      if (strJson.length > 0) {
        temObjJson = JSON.parse(strJson.trim());
        if (!temObjJson.requests) {
          this.enableButtonImport(false);
          this.shadowRoot.querySelector('.confirm-import').classList.add('hide');
          return null;
        }
        this.shadowRoot.querySelector('#text-import').value = JSON.stringify(temObjJson, null, '  ');
        this.enableButtonImport(true);
        return temObjJson;
      }
    } catch (ex) {
      this.enableButtonImport(false);
      this.shadowRoot.querySelector('.confirm-import').classList.add('hide');
      return null;
    }
  }

  executeImport() {
    let dataImport = this.validateInputImport();
    console.log('dataImport', dataImport);
    this.repository.importData(dataImport);
    this.shadowRoot.querySelector('#modalImport').close();
    this.loadConfig(dataImport.configurations);
  }

  executeRequests() {
    this.shadowRoot.querySelector('.view-port-run-exec').classList.add('view-port-run-exec-show');
  }

  async executeRun(index) {
    if (index >= this.requests.length) {
      this.shadowRoot.querySelector('execution-app').enableButtons(true);
      this.shadowRoot.querySelector('execution-app').showResultCount = true;
      this.shadowRoot.querySelector('execution-app').reportBuild();
      return;
    }
    let itemRequest = this.requests[index];
    let item = {
      name: itemRequest.name,
      pending: true,
      itemRequest
    };
    this.shadowRoot.querySelector('execution-app').initRun(index, item);
    let url = this.validateVariables(itemRequest.url);
    let { response, request } = await sendRequest.run(url, itemRequest.method, this.validateJsonVariables(itemRequest.headers), this.validateJsonVariables(itemRequest.body), true);
    item.pending = false;
    item.success = (request.status >= 200 && request.status < 300);
    item.response = response;
    item.request = request;
    this.shadowRoot.querySelector('execution-app').nextItem(index + 1, item);
  }

  closeRun() {
    this.shadowRoot.querySelector('execution-app').disabledReport();
    this.shadowRoot.querySelector('.view-port-run-exec').classList.remove('view-port-run-exec-show');
  }

  async refreshTsec() {
    this.shadowRoot.querySelector('cells-spinner-global-pfco').activeLoading = true;
    await this.shadowRoot.querySelector('#dm-control-access').runEmployeeAuthentication();
    this.shadowRoot.querySelector('cells-spinner-global-pfco').activeLoading = false;
  }

  viewResultText(info) {
    let modal = this.shadowRoot.querySelector('#modalViewResult');
    modal.title = info.tittle;
    this.shadowRoot.querySelector('#text-result').value = info.data;
    modal.open();
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__main">
          <section class = "menu-vertical">
            <menu-app
                      titleApp = "${window.AppConfig.appTitle}"
                      @on-refresh-tsec = ${() => this.refreshTsec()}
                      @on-run = ${() => this.executeRequests() }
                      @on-import = ${() => this.importar() }
                      @on-export = ${() => this.export() }
                      @on-tsec = ${() => this.copyTsec()}
                      @on-config = ${() => this.shadowRoot.querySelector('#modalConfig').open() }
                      @on-selected-item = ${({detail}) => this.selectedItem(detail) }
                      @on-edit-item = ${({detail}) => this.openForm('edit', detail) }
                      @on-add = ${({detail}) => this.openForm('create', detail) } >
            </menu-app>
          </section>
          <main class = "main-section" >
            <request-app
                      @on-view-result-text = ${({detail}) => this.viewResultText(detail)}
                      @on-send-request = ${({detail}) => this.sendRequest(detail) }
                      @on-error-format-json = ${() => this.shadowRoot.querySelector('#toast').showToast({type: 'error', message: 'Formato JSON inválido.' })}
                      @on-save-request = ${({detail}) => this.saveRequest(detail, 'noList')}>
            </request-app>
          </main>

          <cells-modal-pfco
                            id="modalCreate"
                            buttonOk=""
                            buttonCancel=""
                            width = "800px"
                            @on-change-close = ${() => this.shadowRoot.querySelector('form-request-app').reset()}
                            title="Nueva petición">
                        <form-request-app
                              .methods = "${window.AppConfig.methods}"
                               @on-data-form = ${({detail}) => this.saveRequest(detail)}
                               @on-delete = ${({ detail }) => this.deleteRequest(detail)}
                               @on-cancel = ${() => this.shadowRoot.querySelector('#modalCreate').close()}
                               ></form-request-app>
         </cells-modal-pfco>

         <cells-modal-pfco
                            id="modalTsec"
                            buttonOk=""
                            buttonCancel=""
                            width = "800px"
                            title="Tsec">
                            <textarea
                                      readonly
                                      class = "textarea-tsec">${window.sessionStorage.getItem('tsec')}</textarea>
         </cells-modal-pfco>

         <cells-modal-pfco
                            id="modalViewResult"
                            buttonOk=""
                            buttonCancel=""
                            width = "800px"
                            title="view Result">
                            <textarea
                                      readonly
                                      id = "text-result"
                                      class = "textarea-result"></textarea>
         </cells-modal-pfco>

         <cells-modal-pfco
                            id="modalExport"
                            buttonOk=""
                            buttonCancel=""
                            width = "90%"
                            title="Exportar peticiones">
                            <textarea
                                      readonly
                                      id = "text-export"
                                      class = "textarea-expo-impo"></textarea>

                              <div class = "content-actions-import" >
                                  <a  id = "btn-exportar" class = "button primary" >Exportar</a>
                            </div>
         </cells-modal-pfco>


         <cells-modal-pfco
                            id="modalImport"
                            buttonOk=""
                            buttonCancel=""
                            width = "90%"
                            title="Importar peticiones">

                            <textarea
                                      id = "text-import"
                                      @blur = "${(e) => this.validateInputImport()}"
                                      class = "textarea-expo-impo"></textarea>

                            <div class = "content-actions-import" >

                              <div class = "confirm-import hide">
                                La información registrada será modificada. ¿Seguro de continuar?
                                <button @click = ${() => this.executeImport() } >Si</button>
                                <button @click = ${() => this.shadowRoot.querySelector('.confirm-import').classList.add('hide') }>No</button>
                              </div>

                              <div>
                                <button id = "btn-importar" disabled @click = ${() => this.shadowRoot.querySelector('.confirm-import').classList.remove('hide') } class = "button primary disabled" >Importar</button>
                              </div>

                            </div>
         </cells-modal-pfco>

         <cells-modal-pfco
                            id="modalConfig"
                            buttonOk=""
                            buttonCancel=""
                            width = "800px"
                            title="Configuración">
                          <form-config-app
                            @on-data-form = ${({detail}) => this.saveConfig(detail)}
                            @on-cancel = ${() => this.shadowRoot.querySelector('#modalConfig').close() }
                          ></form-config-app>

         </cells-modal-pfco>

         <cells-toast-pfco  id="toast" clsCustom = "custom-bottom-menu"  ></cells-toast-pfco>
         <!-- Access Control  -->
         <cells-access-control-pfco
                              titleApp = "${window.AppConfig.appTitle}"
                              maskVisible
                              appId = "13000105"
                              version = "1.0"
                              srcLogo = "resources/images/logo_bbva_azul.svg"
                              id = "dm-control-access"
                              ></cells-access-control-pfco>

          <cells-spinner-global-pfco></cells-spinner-global-pfco>

          <div class = "view-port-run-exec" >
            <div class = "content-run-exec" >
              <execution-app
                  .total = ${this.requests.length}
                  @on-execution = ${({detail}) => this.executeRun(detail)}
                  @on-cancel-execution = ${() => this.closeRun()}
               ></execution-app>
            </div>
          </div>


          <bbva-help-modal
                class = "modal-error"
                header-icon="coronita:error"
                header-text="Error de credenciales"
                body-text="Los datos de autenticación no se han podido obtener por problemas con los servicios de logueo empleados. Vuelve a intentarlo en unos minutos, si el problema persiste contactate con el personal de soporte.">
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

}

window.customElements.define(MainPage.is, MainPage);