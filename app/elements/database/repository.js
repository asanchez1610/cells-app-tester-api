class Repository {
  constructor() {
    this.storeRequestName = 'requests';
    this.storeConfigurationsName = 'configurations';
    const indexedDB = window.indexedDB;
    if (indexedDB) {
      let request = indexedDB.open('DTesterApp', 1);
      request.onsuccess = () => {
        this.db = request.result;
        this.dispatch('db-on-ready', true);
      };
      request.onupgradeneeded = (event) => {
        this.db = request.result;
        console.log('DTesterApp[create]', this.db);
        let requestOS = this.db.createObjectStore(this.storeRequestName, {
          keyPath: 'id'
        });
        requestOS.createIndex('name', 'name', { unique: true });
        this.db.createObjectStore(this.storeConfigurationsName, {
          keyPath: 'name'
        });
        this.initializeConfig(event);
        this.dispatch('db-on-ready', true);
      };
      request.onerror = (error) => {
        this.dispatch('db-on-error-load', true);
        console.warn('Error DB', error);
      };
    } else {
      alert('indexedDB no soportadi');
    }
  }

  initializeConfig(event) {
    let transaction = event.target.transaction;
    let config = {};
    config.name = 'serverBase';
    config.value = 'https://int-corona-pe.work-02.nextgen.igrupobbva';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'grantingTicket';
    config.value = 'https://int-corona-pe.work-02.nextgen.igrupobbva/TechArchitecture/pe/grantingTicket/V02';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'urlLoginNautilius';
    config.value = 'http://ei-community.grupobbva.com/';
    //config.value = 'https://au-corona-i-pe.work-04.platform.bbva.com/';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'idRequest';
    config.value = '';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'idQuotation';
    config.value = '';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'contactID';
    config.value = '';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'customerId';
    config.value = '';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'documentType';
    config.value = '';
    transaction.objectStore(this.storeConfigurationsName).add(config);
    config = {};
    config.name = 'documentNumber';
    config.value = '';
    transaction.objectStore(this.storeConfigurationsName).add(config);
  }

  dispatch(name, data) {
    const customEvent = new CustomEvent(name, {
      detail: data,
      bubbles: true,
      composed: true
    });
    window.dispatchEvent(customEvent);
  }

  idGenerator() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }

  listRequest() {
    try {
      let request = this.db.transaction([ this.storeRequestName ], 'readwrite').objectStore(this.storeRequestName).getAll();
      request.onsuccess = (event) => {
        this.dispatch('list-request-complete', event.target.result);
      };
      request.onerror = (error) => {
        console.warn('Error al listar Request', error);
        this.dispatch('list-request-complete', []);
      };
    } catch (e) {
      console.warn('Error al listar Request', e);
      this.dispatch('list-request-complete', []);
    }
  }

  listConfigurations() {
    try {
      let request = this.db.transaction([ this.storeConfigurationsName ], 'readonly').objectStore(this.storeConfigurationsName).getAll();
      request.onsuccess = (event) => {
        this.dispatch('list-config-complete', event.target.result);
      };
      request.onerror = (error) => {
        console.warn('Error al listar Configurations', error);
        this.dispatch('list-config-complete', []);
      };
    } catch (e) {
      console.warn('Error al listar Configurations', e);
      this.dispatch('list-config-complete', []);
    }
  }

  saveRequest(data, indicator) {
    if (data.id) {
      this.editRequest(data, indicator);
    } else {
      this.createRequest(data);
    }
  }

  saveConfig(data) {
    let request = this.db.transaction([ this.storeConfigurationsName ], 'readwrite').objectStore(this.storeConfigurationsName).put(data);
    request.onsuccess = () => {
      this.dispatch('on-success-config-save', {});
    };
    request.onerror = (error) => {
      this.dispatch('on-error-config-save', error);
    };
  }

  createRequest(data) {
    data.id = this.idGenerator();
    let request = this.db.transaction([ this.storeRequestName ], 'readwrite').objectStore(this.storeRequestName).add(data);
    request.onsuccess = (event) => {
      this.dispatch('on-success-request-save', {});
      this.listRequest();
    };
    request.onerror = (error) => {
      this.dispatch('on-error-request-save', error);
    };
  }

  editRequest(data, indicator) {
    let request = this.db.transaction([ this.storeRequestName ], 'readwrite').objectStore(this.storeRequestName).put(data);
    request.onsuccess = (event) => {
      if (indicator === 'noList') {
        this.dispatch('on-save-complete', {});
      } else {
        this.listRequest();
      }
      this.dispatch('on-success-request-save', {});
    };
    request.onerror = (error) => {
      this.dispatch('on-error-request-save', error);
    };
  }

  deleteRequest(id) {
    let request = this.db.transaction([ this.storeRequestName ], 'readwrite').objectStore(this.storeRequestName).delete(id);
    request.onsuccess = (event) => {
      this.listRequest();
    };
    request.onerror = (error) => {
      console.warn('Error deleteRequest', error);
      this.dispatch('on-error-delete-request', error);
    };
  }

  importData(data) {
    let requests = data.requests || [];
    let configurations = data.configurations || [];

    let requestsOS = this.db.transaction([ this.storeRequestName ], 'readwrite').objectStore(this.storeRequestName);
    requestsOS.clear();

    requests.forEach(element => {
      requestsOS.add(element);
    });

    if (configurations.length > 0) {
      let configurationsOS = this.db.transaction([ this.storeConfigurationsName ], 'readwrite').objectStore(this.storeConfigurationsName);
      configurationsOS.clear();
      configurations.forEach(element => {
        configurationsOS.add(element);
      });
    }
  }

}
let repository = new Repository();
export { repository };