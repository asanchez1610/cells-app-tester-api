import { LitElement, html, css } from 'lit-element';
import '@cells-components/cells-icon';
import '@bbva-web-components/bbva-form-field';
import '@quotation-components/cells-form-select-pfco';
import stylesGrid from '../styles/grid-styles.js';
import stylesButtons from '../styles/buttons-styles.js';
import '../../elements/utils/behavior.js';

const utilBehavior = CellsBehaviors.DTesterAppBehavior;
class FormRequestApp extends utilBehavior(LitElement) {
  static get styles() {
    return [
      stylesGrid,
      stylesButtons,
      css`
      :host {
        box-sizing: border-box;
        margin:0;
        padding:0;
      }

      :host([hidden]), [hidden] {
        display: none !important;
      }

      main {
        padding: 5px 15px;
      }

      .section-buttons {
        margin-top: 5px;
        border-top : 1px solid #e1e1e1;
        padding: 10px 15px 5px 15px;
        display: flex;
        justify-content: center;
        align-items:center;
      }

      .section-buttons .button{
        margin: 0 2px;
      }
    `
    ];
  }

  static get properties() {
    return {
      methods: {
        type: Array
      },
      dataRequest: Object
    };
  }

  constructor() {
    super();
    this.methods = [];
  }

  reset() {
    this.dataRequest = null;
    this.shadowRoot.querySelector('#cmb-method').clearSelected();
    this.shadowRoot.querySelector('#txt-name').clearInput();
    this.shadowRoot.querySelector('#txt-smc').clearInput();
    this.shadowRoot.querySelector('#txt-url').clearInput();
  }

  getDataForm() {
    let cmbMethod = this.shadowRoot.querySelector('#cmb-method');
    let name = this.shadowRoot.querySelector('#txt-name');
    let smc = this.shadowRoot.querySelector('#txt-smc').value;
    let url = this.shadowRoot.querySelector('#txt-url');
    let data = {};
    let errors = 0;
    if (!cmbMethod.getSelected()) {
      cmbMethod.setAttribute('invalid', 'invalid');
      cmbMethod.invalid = true;
      errors++;
    } else {
      cmbMethod.removeAttribute('invalid', 'invalid');
      cmbMethod.invalid = false;
      data.method = cmbMethod.getSelected().name;
    }
    if (!name.value) {
      name.setAttribute('invalid', 'invalid');
      name.invalid = true;
      errors++;
    } else {
      name.removeAttribute('invalid', 'invalid');
      name.invalid = false;
      data.name = name.value;
    }
    if (!url.value) {
      url.setAttribute('invalid', 'invalid');
      url.invalid = true;
      errors++;
    } else {
      url.removeAttribute('invalid', 'invalid');
      url.invalid = false;
      data.url = url.value;
    }

    data.smc = smc ? smc : '';

    if (this.dataRequest && this.dataRequest.id) {
      data = Object.assign(this.dataRequest, data);
    }

    if (errors === 0) {
      return data;
    }
    return null;
  }

  onSendForm() {
    let dataForm = this.getDataForm();
    console.log('dataForm', dataForm);
    if (dataForm) {
      this.dispatch('on-data-form', dataForm);
      this.dispatch('on-cancel', {});
    }
  }

  setDataRequest(data) {
    this.dataRequest = data;
    this.shadowRoot.querySelector('#cmb-method').setSelected({ name: data.method}, data.method);
    this.requestUpdate();
  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
        <main>
          <div class="section group">
            <div class="col span_3_of_12">
              <cells-form-select-pfco
                                id = "cmb-method"
                                .items = "${this.methods}"
                                pathId = "name"
                                pathText = "name"
                                label = "Método" ></cells-form-select-pfco>
            </div>
            <div class="col span_5_of_12">
              <bbva-form-field .value = "${this.dataRequest && this.dataRequest.name ? this.dataRequest.name : ''}" label = "Nombre" id = "txt-name" ></bbva-form-field>
            </div>
            <div class="col span_4_of_12">
              <bbva-form-field .value = "${this.dataRequest && this.dataRequest.smc ? this.dataRequest.smc : ''}" label = "SMC" id = "txt-smc" ></bbva-form-field>
            </div>
          </div>

          <div class="section group">
            <div class="col span_12_of_12">
              <bbva-form-field .value = "${this.dataRequest && this.dataRequest.url ? this.dataRequest.url : ''}" label = "Url" id = "txt-url" ></bbva-form-field>
            </div>
          </div>

          <section class = "section-buttons">
            <button @click = ${() => this.dispatch('on-cancel', {})} class = "button warning" >Cancelar</button>
            <button @click = ${() => this.dispatch('on-delete', { data: this.dataRequest })} ?hidden = "${!this.dataRequest}" class = "button red" >Eliminar</button>
            <button @click = ${() => this.onSendForm()} class = "button primary" >${this.dataRequest ? 'Editar' : 'Crear'}</button>
          </section>

        </main>
    `;
  }
}
customElements.define('form-request-app', FormRequestApp);