import { css, } from 'lit-element';

export default css`
/* Buttons */
.button, .button:visited {
  display: inline-block;
  padding: 5px 12px;
  color: white;
  text-shadow: 1px 2px 0 rgba(0, 0, 0, 0.1);
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.15);
  transition: all 250ms;
  outline:none;
}

.margin-bottom-panel {
  margin-bottom:2%;
}

.button cells-icon {
  width: 16px;
    height: 16px;
    margin: 0;
    margin-bottom: 2px;
    margin-right:2px;
}

button.button {
    font-size: 1em;
    padding: 11px 12px 9px 12px;
}

/* button large */
.button-small, .button-small:visited {
  padding: 2px 15px;
  font-size:0.85em;
}

.button-small cells-icon {
    width: 13px;
    height: 13px;
    margin: 0;
    margin-bottom: 2px;
    margin-right:2px;
}

button.button-small {
  font-size:0.85em;
    padding: 8px 10px 6px 10px;
}

/* button large */
.button-large, .button-large:visited {
  padding: 11px 24px 10px 22px;
    font-size: 1em;
}

.button-large cells-icon {
    width: 18px;
    height: 18px;
    margin: 0;
    margin-bottom: 2px;
    margin-right:2px;
}

button.button-large {
     padding: 11px 24px 10px 22px;
    font-size: 1em;
}

.button.info {
  background-color: #09f;
}
.button.info:hover, .button.info:hover {
  background-color: #4db8ff;
}
.button.info:active {
  background-color: #80ccff;
}
.button.shadow-blue {
  background-color: #469;
}
.button.shadow-blue:hover, .button.shadow-blue:hover {
  background-color: #6c8dbe;
}
.button.shadow-blue:active {
  background-color: #8fa8cd;
}
.button.black {
  background-color: #222;
}
.button.black:hover, .button.black:hover {
  background-color: #484848;
}
.button.black:active {
  background-color: #626262;
}
.button.red-light {
  background-color: #d43;
}
.button.red-light:hover, .button.red-light:hover {
  background-color: #e88075;
}
.button.red-light:active {
  background-color: #efa8a0;
}
.button.blue {
  background-color: #07d;
}
.button.blue:hover, .button.blue:hover {
  background-color: #2b9dff;
}
.button.blue:active {
  background-color: #5eb4ff;
}
.button.red {
  background-color: #c22;
}
.button.red:hover, .button.red:hover {
  background-color: #e35757;
}
.button.red:active {
  background-color: #ea8383;
}
.button.primary {
  background-color: #1464A5;
}
.button.primary:hover, .button.primary:hover {
  background-color: #1973B8;
}
.button.primary:active {
  background-color: #1973B8;
}
.button.secundary {
  border: 1px solid #e1e1e1;
  color:#666;
  text-shadow:none;
  background-color: #fff;
}
.button.secundary:hover, .button.secundary:hover {
  background-color: #f4f4f4;
}
.button.secundary:active {
  background-color: #f4f4f4;
}
.button.success {
  background-color: #388D4F;
}
.button.success:hover, .button.success:hover {
  background-color: #48AE64;
}
.button.success:active {
  background-color: #48AE64;
}
.button:focus, .button:hover {
  cursor: pointer;
}

.button.warning {
  background-color: #C65302;
}
.button.warning:hover, .button.warning:hover {
  background-color: #D8732C;
}
.button.warning:active {
  background-color: #D8732C;
}

.full-width {
  flex: 1;
    text-align: center;
}

.disabled {
  opacity: 0.3; }

  a.button {
    padding: 10px 20px;
    text-decoration: none;
  }
`;
