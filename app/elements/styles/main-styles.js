import { css, } from 'lit-element';


export default css`

* {
  margin:0;
  padding:0;
}

.menu-vertical {
  width:300px;
  position:relative;
}

.main-section {
  min-height: 100vh;
  width: calc(100% - 280px);
  margin-left: 280px;
}

.textarea-tsec {
        width: calc(100% - 20px);
        height: 410px;
        color:green;
        background-color: #353A35;
        border:none;
        outline:none;
        padding: 10px;
        font-size:15px;
        font-weight:bold;
        resize: none;
}

.textarea-result {
        width: calc(100% - 20px);
        height: 520px;
        color:#F8CD51;
        background-color: #353A35;
        border:none;
        outline:none;
        padding: 10px;
        font-size:15px;
        font-weight:bold;
        resize: none;
}

.textarea-expo-impo {
        width: calc(100% - 20px);
        height: 450px;
        color:#5BBEFF;
        background-color: #353A35;
        border:none;
        outline:none;
        padding: 10px;
        font-size:15px;
        font-weight:bold;
        resize: none;
}

.content-actions-import {
  width:100%;
  padding: 10px;
  display:flex;
  justify-content: center;
  align-items:center;
}

.content-actions-import div{
  margin:5px;
}

.confirm-import {
  font-size:13px;
}

.confirm-import button{
  font-size:12px;
  padding: 3px 10px;
}

.confirm-import button:first-child{
  margin-right:5px;
  margin-left:5px;
}

.hide {
  display:none;
}

.view-port-run-exec {
  position:fixed;
  width: 100%;
  height: 100vh;
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.8); /* Black w/ opacity */
  top:0;
  left:0;
  z-index: -1;
}

.view-port-run-exec execution-app {
  display: none;
}

.view-port-run-exec-show {
  z-index: 300;
}

.view-port-run-exec-show execution-app {
  display: block;
}

.content-run-exec {
  background-color:#fff;
  margin: 25px auto;
  width: 720px;
}

.modal-error {
  --bbva-help-modal-header-icon-color: #C0475E;
}

/* Responsive */
  @media only screen and (max-width: 480px) {

  }

`;