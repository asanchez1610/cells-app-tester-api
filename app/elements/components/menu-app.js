import { LitElement, html, css } from 'lit-element';
import '@cells-components/cells-icon';
import '@polymer/paper-tooltip/paper-tooltip';
import '@bbva-web-components/bbva-form-field';
import '../../elements/utils/behavior.js';

const utilBehavior = CellsBehaviors.DTesterAppBehavior;
class MenuApp extends utilBehavior(LitElement) {
  static get styles() {
    return css`
      :host {
        display: flex;
        box-sizing: border-box;
        z-index: 10;
        position: fixed;
        top: 0;
        left: 0;
        margin:0;
        padding:0;
      }

      :host([hidden]), [hidden] {
        display: none !important;
      }

      .container-menu {
        width:280px;
        height: 100vh;
        background-color: #2B2C2A;
        border-right: 1px solid #353A35;
      }
      .header-menu {
        color: #fff;
        background-color: #004481;
        height: 48px;
        font-size: 17px;
        padding: 0 10px;
        display: flex;
        flex-flow: row wrap;
        justify-content: space-between;
        align-items:center;
      }
      .icon-logo {
        top:-1px;
        position: relative;
        margin-right:5px;
        width: 21px;
        height: 21px;
      }

      .icon-add-request {
        top:-1px;
        position: relative;
        width: 20px;
        height: 20px;
        cursor:pointer;
        transition: all 0.3s ease-in-out;
      }

      .icon-refresh-tsec {
        top:-1px;
        position: relative;
        width: 20px;
        height: 20px;
        cursor:pointer;
        margin-right:7px;
        transition: all 0.3s ease-in-out;
      }

      .icon-refresh-tsec:hover {
        color:#7C6AC7;
      }

      .icon-add-request:hover {
        color:#5AC4C4;
      }

      .tooltip-custom {
        --paper-tooltip-background:blue;
      }

      .request-items {
        height: calc(100vh - 136px);
        overflow-y: auto;
        overflow-x:hidden;
      }

      .request-items ul{
        list-style: none; /* Remove list bullets */
        padding: 0;
        margin: 0;
        color:#fff;
      }

      .request-items li{
        height:40px;
        font-size: 12px;
        display:flex;
        flex-flow: row wrap;
        justify-content: space-between;
        align-items:center;
        padding: 0 10px;
        cursor:pointer;
        transition: all 0.2s ease-in-out;
      }

      .request-items li:hover{
        background-color: #353A35;
      }

      .active{
        background-color: #353A35;
      }

      .method {
        margin-right: 10px;
      }

      .get {
        color: #8F7AE5;
      }

      .post {
        color: #388D4F;
      }

      .put {
        color:#F7893B;
      }

      .patch {
        color:#B79E5E;
      }

      .delete {
        color:#DA3851;
      }

      .options {
        color:#1973B8;
      }

      .head {
        color:#49A5E6;
      }

      .foot-menu {
        position: fixed;
        bottom: 0;
        left:0;
        height: 40px;
        width:280px;
        display: grid;
        grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
      }

      .foot-menu div{
        display:flex;
        font-size:10px;
        color: #fff;
        justify-content: center;
        align-items:center;
      }

      .btn-foot {
        background-color: #1464A5;
        cursor: pointer;
        transition: all 0.3s ease-in-out;
        border-right: 1px solid #004481;
      }

      .btn-foot:last-child {
        border-right: none;
      }

      .btn-foot:hover {
        background-color: #004481;
      }

      .btn-foot cells-icon{
        width:20px;
        height:20px;
      }

      .action-request-icon {
        width: 12px;
        height: 12px;
        color: #999;
        transition: all 0.3s ease-in-out;
        cursor: pointer;
      }

      .action-request-icon:hover {
        color: #fff;
      }

      .hide {
        display:none !important;
      }

    `;
  }

  static get properties() {
    return {
      titleApp: String,
      items: Array
    };
  }

  constructor() {
    super();
    this.titleApp = 'Rest App';
  }

  selectedItem(id, item) {
    let target = this.shadowRoot.querySelector(`#${id}`);
    let itemsRequest = this.shadowRoot.querySelectorAll('.item') || [];
    itemsRequest.forEach(record => record.classList.remove('active'));
    target.classList.add('active');
    this.dispatch('on-selected-item', item);
  }

  onEdit(e, item) {
    e.stopPropagation();
    this.dispatch('on-edit-item', item);
  }

  onAdd() {
    this.dispatch('on-add', {});
  }

  onFootAction(action) {
    switch (action) {
      case 'export':
        this.dispatch('on-export', {});
        break;
      case 'import':
        this.dispatch('on-import', {});
        break;
      case 'config':
        this.dispatch('on-config', {});
        break;
      case 'tsec':
        this.dispatch('on-tsec', {});
        break;
      case 'run':
        this.dispatch('on-run', {});
        break;
    }
  }

  clearSelectedItems() {
    let itemsRequest = this.shadowRoot.querySelectorAll('.item') || [];
    itemsRequest.forEach(record => record.classList.remove('active'));
    this.shadowRoot.querySelector('#txt-search').value = '';
  }

  searchRequestView(e) {
    let value = this.shadowRoot.querySelector('#txt-search').value;
    if (typeof value === 'undefined') {
      value = '';
    }
    value = value.toUpperCase();
    let items = this.shadowRoot.querySelectorAll('.item') || [];
    items.forEach(item => {
      if ((item.dataset.filtro || '').toUpperCase().indexOf(value) >= 0) {
        item.classList.remove('hide');
      } else {
        item.classList.add('hide');
      }
    });
  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
      <main class = "container-menu">
        <section class = "header-menu">
          <div>
            <cells-icon icon = "coronita:channels" class = "icon-logo" ></cells-icon> ${this.titleApp}
          </div>
          <div>
            <cells-icon id = "btn-refresh-tsec" @click = ${() => this.dispatch('on-refresh-tsec', {})} icon = "coronita:historic" class = "icon-refresh-tsec"></cells-icon>
            <paper-tooltip for="btn-refresh-tsec" animationDelay = "300" position = "bottom" ><div style = "width:73px;text-align:center;">Actualizar TSEC</div></paper-tooltip>
            <cells-icon id = "btn-add-request" @click = ${this.onAdd} icon = "coronita:add" class = "icon-add-request"></cells-icon>
            <paper-tooltip for="btn-add-request" animationDelay = "300" position = "right" ><div style = "width:72px;">Nueva petición</div></paper-tooltip>
          </div>
        </section>
        <section>
          <bbva-form-field id = "txt-search" label = "Buscar" @keyup = ${(e) => this.searchRequestView(e) } ></bbva-form-field>
          <nav class = "request-items">
            <ul>
              ${(this.items || []).map((item, index) => html`
                <li id ="item-request-${index}" data-filtro = "${item.name}" class = "item" @click = ${(e) => this.selectedItem(`item-request-${index}`, item)}>
                  <div>
                    <span class = "method ${item.method.toLowerCase()}">${item.method.toUpperCase()}</span>${item.name}
                  </div>
                  <div>
                    <cells-icon @click = ${(e) => this.onEdit(e, item)} id = "btn-edit-${index}" class = "action-request-icon" icon = "coronita:edit"></cells-icon>
                    <paper-tooltip for="btn-edit-${index}" animationDelay = "300" position = "right" >Editar</paper-tooltip>
                  </div>
                </li>`)}
                <div ?hidden = "${(this.items || []).length > 0}" style = "font-style: italic;text-align:center;padding: 18px 10px;font-size:0.85em;">
            No hay peticiones para mostrar.
          </div>
            </ul>
          </nav>

        </section>
        <section class = "foot-menu">
          <div id = "btn-export" class = "btn-foot" @click = ${() => this.onFootAction('export')}><cells-icon icon = "coronita:return-15" ></cells-icon></div>
          <div id = "btn-import" class = "btn-foot" @click = ${() => this.onFootAction('import')}><cells-icon icon = "coronita:down" ></cells-icon></div>
          <div id = "btn-config" class = "btn-foot" @click = ${() => this.onFootAction('config')}><cells-icon icon = "coronita:settings" ></cells-icon></div>
          <div id = "btn-tsec" class = "btn-foot" @click = ${() => this.onFootAction('tsec')}><cells-icon icon = "coronita:token" ></cells-icon></div>
          <div id = "btn-run" class = "btn-foot" @click = ${() => this.onFootAction('run')}><cells-icon icon = "coronita:play" ></cells-icon></div>

          <paper-tooltip for="btn-export" offset = "2" animationDelay = "300" position = "top" >Exportar</paper-tooltip>
          <paper-tooltip for="btn-import" offset = "2" animationDelay = "300" position = "top" >Importar</paper-tooltip>
          <paper-tooltip for="btn-config" offset = "2" animationDelay = "300" position = "top" >Configuración</paper-tooltip>
          <paper-tooltip for="btn-tsec" offset = "2" animationDelay = "300" position = "top" >Obtener Tsec</paper-tooltip>
          <paper-tooltip for="btn-run" offset = "2" animationDelay = "300" position = "top" ><div style = "width:130px;">Ejecutar pruebas masivas</div></paper-tooltip>

        </section>
      </main>
    `;
  }
}
customElements.define('menu-app', MenuApp);