import { LitElement, html, css } from 'lit-element';
import '@cells-components/cells-icon';
import '@bbva-web-components/bbva-form-field';
import '../../elements/utils/behavior.js';
import '@alenaksu/json-viewer';

const utilBehavior = CellsBehaviors.DTesterAppBehavior;
class RequestApp extends utilBehavior(LitElement) {
  static get styles() {
    return css`
      :host {
        box-sizing: border-box;
        margin:0;
        padding:0;
        position:relative;
      }

      :host([hidden]), [hidden] {
        display: none !important;
      }

      header {
        width:100%;
        height:48px;
        display:flex;
      }

      .container-request {
        width:100%;
        height: calc(100vh - 48px);
        background-color: #2B2C2A;
        color: #fff;
        display: grid;
        grid-template-columns: 1fr 1fr;
      }

      .container-request section {
        border-right: 1px solid #2B2C2A;
        height: calc(100vh - 98px);
      }

      .container-request section:last-child {
        border-right: none;
      }

      .method {
        border-right: 1px solid #c7c1c1;
        width:150px;
      }

      .endPoint {
        border-right: 1px solid #c7c1c1;
        width: calc(100% - 350px);
      }

      .btn-enviar {
        width:100px;
      }

      .btn-guardar {
        width:100px;
      }

      .button, .button:visited {
        display: inline-block;
        padding: 5px 12px;
        color: white;
        text-shadow: 1px 2px 0 rgba(0, 0, 0, 0.1);
        border: 1px solid rgba(0, 0, 0, 0.1);
        transition: all 250ms;
        outline:none;
        cursor: pointer;
        font-size:13px
      }

      .button.shadow-blue {
        background-color: #469;
      }
      .button.shadow-blue:hover, .button.shadow-blue:hover {
        background-color: #6c8dbe;
      }
      .button.shadow-blue:active {
        background-color: #8fa8cd;
      }

      .button.info {
        background-color: #09f;
      }
      .button.info:hover, .button.info:hover {
        background-color: #4db8ff;
      }
      .button.info:active {
        background-color: #80ccff;
      }

      .button.red-light {
        background-color: #d43;
      }
      .button.red-light:hover, .button.red-light:hover {
        background-color: #e88075;
      }
      .button.red-light:active {
        background-color: #efa8a0;
      }

      .button.success {
        background-color: #388D4F;
      }
      .button.success:hover, .button.success:hover {
        background-color: #48AE64;
      }
      .button.success:active {
        background-color: #48AE64;
      }

      .disabled {
        background-color: #ccc !important;
        opacity: 0.7;
      }

      #tab1:checked ~ section .tab1,
      #tab2:checked ~ section .tab2,
      #tab3:checked ~ section .tab3,
      #tab4:checked ~ section .tab4,
      #tab5:checked ~ section .tab5 {
        display: block;
      }

      #tab1:checked ~ nav .tab1,
      #tab2:checked ~ nav .tab2,
      #tab3:checked ~ nav .tab3,
      #tab4:checked ~ nav .tab4,
      #tab5:checked ~ nav .tab5 {
        color: #ccc;
      }

      .pc-tab > input,
      .pc-tab section > div {
        display: none;
      }
      .pc-tab {
        margin: 0 auto;
      }
      .pc-tab ul {
        list-style: none;
        margin: 0;
        padding: 0;
      }
      .pc-tab ul li label {
        float: left;
        height: 48px;
        padding: 0px 15px;
        line-height:48px;
        border-right: 1px solid #353A35;
        background: #2B2C2A;
        color: #fff;
      }
      .pc-tab ul li label:hover {
        background: #353A35;
      }
      .pc-tab ul li label:active {
        background: #353A35;
      }
      .pc-tab ul li:not(:last-child) label {
        border-right-width: 0;
      }
      .pc-tab section {
        clear: both;
      }
      .pc-tab section div {
        padding: 0px;
        width: 100%;
        border: none;
        background: #353A35;
        line-height: 1.5em;
        letter-spacing: 0.3px;
        color: #fff;
        height: calc(100vh - 98px);
      }
      .pc-tab section div h2 {
        margin: 0;
        letter-spacing: 1px;
        color: #fff;
      }

      #tab1:checked ~ nav .tab1 label,
      #tab2:checked ~ nav .tab2 label,
      #tab3:checked ~ nav .tab3 label,
      #tab4:checked ~ nav .tab4 label,
      #tab5:checked ~ nav .tab5 label {
        background: #353A35;
        color: #fff;
        position: relative;
      }
      #tab1:checked ~ nav .tab1 label:after,
      #tab2:checked ~ nav .tab2 label:after,
      #tab3:checked ~ nav .tab3 label:after,
      #tab4:checked ~ nav .tab4 label:after,
      #tab5:checked ~ nav .tab5 label:after {
        content: '';
        display: block;
        position: absolute;
        height: 2px;
        width: 100%;
        background: #353A35;
        left: 0;
        bottom: -1px;
      }

      textarea {
        width: calc(100% - 20px);
        height: calc(100vh - 122px);
        color:#8F7AE5;
        background-color: #353A35;
        border:none;
        outline:none;
        padding: 10px;
        font-size:15px;
        font-weight:bold;
        resize: none;
      }

      .text-response {
        color: yellow;
      }

      json-viewer {
          /* Background, font and color */
          font-size: 13px;
          padding-top: 15px;
          margin: 0 10px;
          --background-color: #353A35;
          --color: #f8f8f2;
          --font-family: monaco, Consolas, 'Lucida Console', monospace;

          /* Types colors */
          --string-color: #a3eea0;
          --number-color: #d19a66;
          --boolean-color: #4ba7ef;
          --null-color: #df9cf3;
          --property-color: #6fb3d2;

          /* Collapsed node preview */
          --preview-color: rgba(222, 175, 143, 0.9);
      }

      .content-json-viewer {
        position: relative;
        height: calc(100vh - 97px);
        overflow-x:hidden;
        overflow-y:auto;
      }

      .content-json-viewer a{
        position: absolute;
        display:none;
        color: #ccc;
        top: 3px;
        right:15px;
        font-size:12px;
        transition: all 0.2s;
        cursor: pointer;
      }

      .show {
        display: block !important;
      }

      .content-json-viewer a:hover{
        color: #fff;
      }

      .info-result {
        position: absolute;
        top:20px;
        right:15px;
        font-size:12px;
      }

      .status-info {
        padding: 0px 5px;
        margin-right:1px;
      }

      .status-time {
        padding: 0px 5px;
        color:#B6A8EE;
      }

      .status-200 {
        color:limegreen;
      }

      .status-500 {
        color:#E77D8E;
      }

      .status-other {
        color:#F7893B;
      }

    `;
  }

  static get properties() {
    return {
      dataRequest: Object,
      isGet: Boolean,
      showStatus: Boolean,
      jsonResponseStr: String,
      jsonHeadersStr: String
    };
  }

  constructor() {
    super();
    this.dataRequest = {};
  }

  hideVieTextResponse(show) {
    let items = this.shadowRoot.querySelectorAll('.view-text-response') || [];
    items.forEach(element => {
      if (show) {
        element.classList.add('show');
      } else {
        element.classList.remove('show');
      }
    });
  }

  showResponse(response, request, timeExecution) {
    this.hideVieTextResponse(true);
    this.showStatus = true;
    this.shadowRoot.querySelector('.status-time').innerHTML = `${timeExecution} ms`;
    try {
      this.shadowRoot.querySelector('#json-response').data = response;
      this.jsonResponseStr = response;
    } catch (error) {
      console.log(error);
    }
    let jsonHeaders = {};
    try {
      console.log(request);
      if (request.status >= 200 && request.status < 300) {
        this.shadowRoot.querySelector('.status-info').style.color = 'limegreen';
        if (request.status === 204) {
          this.shadowRoot.querySelector('.status-info').innerHTML = `${request.status} ${request.statusText}`;
        } else {
          this.shadowRoot.querySelector('.status-info').innerHTML = `${request.status} Ok`;
        }
      } else if (request.status === 500) {
        this.shadowRoot.querySelector('.status-info').style.color = '#E77D8E';
        this.shadowRoot.querySelector('.status-info').innerHTML = `${request.status} ${request.statusText}`;
      } else {
        this.shadowRoot.querySelector('.status-info').style.color = '#F7893B';
        this.shadowRoot.querySelector('.status-info').innerHTML = `${request.status} ${request.statusText}`;
      }
      request.headers.forEach((value, name) => {
        if (name !== 'tsec') {
          jsonHeaders[name] = value;
        }
      });
      this.jsonHeadersStr = jsonHeaders;
      this.shadowRoot.querySelector('#json-headers-response').data = jsonHeaders;
    } catch (error) {
      console.log(error);
    }
  }

  setRequest(item) {
    this.dataRequest = item;
    this.showStatus = false;
    this.shadowRoot.querySelector('#json-response').data = {};
    this.shadowRoot.querySelector('#json-headers-response').data = {};
    this.isGet = (this.dataRequest.method.toUpperCase() === 'GET');
    this.shadowRoot.querySelector('#tab1').removeAttribute('checked');
    this.shadowRoot.querySelector('#tab1').checked = false;
    this.shadowRoot.querySelector('#tab2').removeAttribute('checked');
    this.shadowRoot.querySelector('#tab2').checked = false;
    this.shadowRoot.querySelector('#tab3').removeAttribute('checked');
    this.shadowRoot.querySelector('#tab3').checked = false;
    this.shadowRoot.querySelector('#tab4').removeAttribute('checked');
    this.shadowRoot.querySelector('#tab4').checked = false;
    this.shadowRoot.querySelector('#tab5').removeAttribute('checked');
    this.shadowRoot.querySelector('#tab5').checked = false;
    if (this.isGet) {
      this.shadowRoot.querySelector('#tab2').setAttribute('checked', 'checked');
      this.shadowRoot.querySelector('#tab2').checked = true;
    } else {
      this.shadowRoot.querySelector('#tab1').setAttribute('checked', 'checked');
      this.shadowRoot.querySelector('#tab1').checked = true;
    }
    this.shadowRoot.querySelector('#tab4').setAttribute('checked', 'checked');
    this.shadowRoot.querySelector('#tab4').checked = true;
    this.shadowRoot.querySelector('#btn-enviar').removeAttribute('disabled');
    this.shadowRoot.querySelector('#btn-enviar').classList.remove('disabled');
    this.shadowRoot.querySelector('#btn-guardar').removeAttribute('disabled');
    this.shadowRoot.querySelector('#btn-guardar').classList.remove('disabled');
    let textareas = (this.shadowRoot.querySelectorAll('textarea') || []);
    textareas.forEach(textarea => textarea.removeAttribute('disabled', 'disabled'));
    if (this.dataRequest.body) {
      this.shadowRoot.querySelector('#json-body').value = JSON.stringify(this.dataRequest.body, null, '  ');
    }
    if (this.dataRequest.headers) {
      this.shadowRoot.querySelector('#json-headers').value = JSON.stringify(this.dataRequest.headers, null, '  ');
    }
    let addParams = this.parseUrlParams(item.url);
    let params = this.dataRequest.params || {};
    let showParams = Object.assign(params, addParams);
    this.shadowRoot.querySelector('#json-params').value = JSON.stringify(showParams, null, '  ');
  }

  clear() {
    this.showStatus = false;
    this.dataRequest = {};
    this.shadowRoot.querySelector('#btn-enviar').setAttribute('disabled', 'disabled');
    this.shadowRoot.querySelector('#btn-enviar').classList.add('disabled');
    this.shadowRoot.querySelector('#btn-guardar').setAttribute('disabled', 'disabled');
    this.shadowRoot.querySelector('#btn-guardar').classList.add('disabled');
    this.shadowRoot.querySelector('#txt-method').clearInput();
    this.shadowRoot.querySelector('#txt-url').clearInput();
    let textareas = (this.shadowRoot.querySelectorAll('textarea') || []);
    textareas.forEach(item => item.value = '');
    textareas.forEach(item => item.setAttribute('disabled', 'disabled'));
    this.hideVieTextResponse();
  }

  blurTextArea(idtextArea) {
    this.shadowRoot.querySelector(`#${idtextArea}`).setAttribute('readonly', 'readonly');
    try {
      let strJson = this.shadowRoot.querySelector(`#${idtextArea}`).value;
      if (strJson.length > 0) {
        let temObjJson = JSON.parse(strJson.trim());
        this.shadowRoot.querySelector(`#${idtextArea}`).value = JSON.stringify(temObjJson, null, '  ');
      }
      if (idtextArea === 'json-params') {
        let params = this.parseJsonObject('json-params');
        let inputUrl = this.shadowRoot.querySelector('#txt-url').value;
        if (this.isNotEmpty(params) && this.isNotEmpty(inputUrl) && inputUrl.indexOf('?') >= 0) {
          let url = inputUrl.substring(0, inputUrl.indexOf('?'));
          let newUrl = this.getUrlParams(url, params);
          this.shadowRoot.querySelector('#txt-url').value = newUrl;
        }
      }
    } catch (e) {
      this.dispatch('on-error-format-json', {});
    }
  }

  parseJsonObject(idtextArea) {
    try {
      let strJson = this.shadowRoot.querySelector(`#${idtextArea}`).value;
      return JSON.parse(strJson.trim());
    } catch (e) {
      console.warn('parseJsonObject', e);
    }
    return {};
  }

  saveRequest() {
    let body = this.parseJsonObject('json-body');
    let headers = this.parseJsonObject('json-headers');
    let params = this.parseJsonObject('json-params');
    let addData = { body, headers, params };
    this.dataRequest.url = this.shadowRoot.querySelector('#txt-url').value;
    let data = Object.assign(this.dataRequest, addData);
    this.dispatch('on-save-request', data);
  }

  sendRequest() {
    let body = this.parseJsonObject('json-body');
    let headers = this.parseJsonObject('json-headers');
    let params = this.parseJsonObject('json-params');
    let addData = { body, headers, params };
    this.dataRequest.url = this.shadowRoot.querySelector('#txt-url').value;
    let data = Object.assign(this.dataRequest, addData);
    this.dispatch('on-send-request', data);
  }

  showViewResultText(type) {
    let jsonStr = '';
    if (type === 'response') {
      try {
        if (this.jsonResponseStr) {
          jsonStr = JSON.stringify(this.jsonResponseStr, null, '  ');
        }
      } catch (e) {
        console.log(e);
      }
      this.dispatch('on-view-result-text', {
        tittle: 'Response',
        data: jsonStr
      });
    } else if (type === 'headers') {
      try {
        if (this.jsonHeadersStr) {
          jsonStr = JSON.stringify(this.jsonHeadersStr, null, '  ');
        }
      } catch (e) {
        console.log(e);
      }
      this.dispatch('on-view-result-text', {
        tittle: 'Headers',
        data: jsonStr
      });
    }
  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
      <header>
        <bbva-form-field id = "txt-method" .value = "${this.dataRequest.method}" readonly class = "method" label = "Método"></bbva-form-field>
        <bbva-form-field id = "txt-url" .value = "${this.dataRequest.url}" readonly class = "endPoint" label = "URL"></bbva-form-field>
        <button @click = ${() => this.sendRequest()} id = "btn-enviar" disabled class = "btn-enviar button shadow-blue disabled">Enviar</button>
        <button @click = ${() => this.saveRequest()} id = "btn-guardar" disabled class = "btn-guardar button success disabled">Guardar</button>
      </header>
      <main class = "container-request">
        <section>
          <!-- Tabs request -->
          <div class="pc-tab">
            <input ?hidden = "${this.isGet}" checked id="tab1" type="radio" name="pct" />
            <input id="tab2" type="radio" name="pct" />
            <input id="tab3" type="radio" name="pct" />
            <nav>
              <ul>
                <li ?hidden = "${this.isGet}" class="tab1">
                  <label for="tab1">Body</label>
                </li>
                <li class="tab2">
                  <label for="tab2">Headers</label>
                </li>
                <li class="tab3">
                  <label for="tab3">Params</label>
                </li>
              </ul>
            </nav>
            <section>
              <div ?hidden = "${this.isGet}" class="tab1">
                <textarea
                          disabled
                          @blur = ${() => this.blurTextArea('json-body')}
                          @click = ${() => this.shadowRoot.querySelector('#json-body').removeAttribute('readonly')}
                          id = "json-body"
                          readonly ></textarea>
              </div>
              <div class="tab2">
                <textarea disabled @blur = ${() => this.blurTextArea('json-headers')} @click = ${() => this.shadowRoot.querySelector('#json-headers').removeAttribute('readonly')} id = "json-headers" readonly ></textarea>
              </div>
              <div class="tab3">
              <textarea disabled @blur = ${() => this.blurTextArea('json-params')} @click = ${() => this.shadowRoot.querySelector('#json-params').removeAttribute('readonly')} id = "json-params" readonly></textarea>
              </div>
            </section>
          </div>
        <!-- tabs request end -->
        </section>

        <section>
          <!-- Tabs request -->
          <div class="pc-tab" style = "position:relative;">
            <div class = "info-result">
              <span ?hidden = "${!this.showStatus}" class = "status-info"></span>
              <span ?hidden = "${!this.showStatus}" class = "status-time"></span>
            </div>
            <input checked id="tab4" type="radio" name="pct2" />
            <input id="tab5" type="radio" name="pct2" />
            <nav>
              <ul>
                <li class="tab4">
                  <label for="tab4">Response</label>
                </li>
                <li class="tab5">
                  <label for="tab5">Headers</label>
                </li>
              </ul>
            </nav>
            <section>
              <div class="tab4 content-json-viewer">
                <a @click = ${() => this.showViewResultText('response')} class = "view-text-response" >Response text</a>
                <json-viewer ?hidden = "${!this.showStatus}" id="json-response"></json-viewer>
              </div>
              <div class="tab5 content-json-viewer">
              <a @click = ${() => this.showViewResultText('headers')} class = "view-text-response" >Headers text</a>
                <json-viewer ?hidden = "${!this.showStatus}" id="json-headers-response"></json-viewer>
              </div>
            </section>
          </div>
        <!-- tabs request end -->
        </section>
      </main>
    `;
  }
}
customElements.define('request-app', RequestApp);