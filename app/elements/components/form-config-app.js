import { LitElement, html, css } from 'lit-element';
import '@bbva-web-components/bbva-form-field';
import stylesGrid from '../styles/grid-styles.js';
import stylesButtons from '../styles/buttons-styles.js';
import '../utils/behavior.js';

const utilBehavior = CellsBehaviors.DTesterAppBehavior;
class FormConfigApp extends utilBehavior(LitElement) {
  static get styles() {
    return [
      stylesGrid,
      stylesButtons,
      css`
      :host {
        box-sizing: border-box;
        margin:0;
        padding:0;
      }

      :host([hidden]), [hidden] {
        display: none !important;
      }

      main {
        padding: 5px 15px;
      }

      .section-buttons {
        margin-top: 5px;
        border-top : 1px solid #e1e1e1;
        padding: 10px 15px 5px 15px;
        display: flex;
        justify-content: center;
        align-items:center;
      }

      .section-buttons .button{
        margin: 0 2px;
      }
    `
    ];
  }

  static get properties() {
    return {
      configurations: {
        type: Array
      }
    };
  }

  constructor() {
    super();
    this.configurations = [];
    this.configurations.push({ name: 'urlBase', value: 'http://localhost/value' });
  }

  getDataForm() {
    let data = [];
    let errors = 0;
    let configItems = this.shadowRoot.querySelectorAll('.item-config') || [];
    configItems.forEach(element => {
      if (this.isEmpty(element.value) && element.classList.contains('requerid')) {
        element.setAttribute('invalid', 'invalid');
        element.invalid = true;
        errors++;
      } else {
        data.push({ name: element.label, value: element.value });
      }
    });
    if (errors === 0) {
      return data;
    }
    return null;
  }

  onSendForm() {
    let dataForm = this.getDataForm();
    console.log('dataFormConfig', dataForm);
    if (dataForm) {
      this.dispatch('on-data-form', dataForm);
      this.dispatch('on-cancel', {});
    }
  }

  setDataConfiguration(data) {
    this.configurations = data;
  }

  get viewConfigTpl() {

    let principals = (this.configurations || []).filter(config => (config.name === 'grantingTicket' || config.name === 'serverBase' || config.name === 'urlLoginNautilius'));
    let others = (this.configurations || []).filter(config => (config.name !== 'grantingTicket' && config.name !== 'serverBase' && config.name !== 'urlLoginNautilius'));

    return html`
      ${(principals || []).map((item) => html`
                    <div class="section group">
                      <div class="col span_12_of_12">
                        <bbva-form-field class = "item-config requerid"  label = "${item.name}" value = "${item.value}" ></bbva-form-field>
                      </div>
                    </div>`)}

                    <div class="section group">
                        ${(others || []).filter((element, index) => index < 3).map((item) => html`
                            <div class="col span_4_of_12">
                              <bbva-form-field class = "item-config"  label = "${item.name}" value = "${item.value}" ></bbva-form-field>
                            </div>`)}
                    </div>

                    <div class="section group">
                        ${(others || []).filter((element, index) => index >= 3).map((item) => html`
                            <div class="col span_4_of_12">
                              <bbva-form-field class = "item-config"  label = "${item.name}" value = "${item.value}" ></bbva-form-field>
                            </div>`)}
                    </div>
      `;
  }

  // Implement `render` to define a template for your element.
  render() {
    return html`
        <main>
            ${this.viewConfigTpl}
          <section class = "section-buttons">
            <button @click = ${() => this.dispatch('on-cancel', {})} class = "button warning" >Cancelar</button>
            <button @click = ${() => this.onSendForm()} class = "button primary" >Modificar</button>
          </section>
        </main>
    `;
  }
}
customElements.define('form-config-app', FormConfigApp);