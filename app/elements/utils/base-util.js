export class BaseUtil {

  constructor() { }

  dispatch(name, data) {
    const customEvent = new CustomEvent(name, {
      detail: data,
      bubbles: true,
      composed: true
    });
    window.dispatchEvent(customEvent);
  }

}