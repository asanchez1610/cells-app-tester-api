import { BaseUtil } from './base-util.js';
class SendRequest extends BaseUtil {

  constructor() {
    super();
  }

  async executeReuest(requestFetch) {
    return new Promise(function(resolve, reject) {
      fetch(requestFetch).then(response => {
        resolve(response);
      }).catch(e => reject(e));
    });
  }

  loading(enable, noLoading) {
    if (!noLoading) {
      this.dispatch('on-loading-mask', enable);
    }
  }

  async run(url, method = 'GET', headers = {}, body = {}, noLoading) {
    if (url) {
      this.loading(true, noLoading);
      let headersSend = {};
      if (!headers['Content-Type']) {
        headersSend['Content-Type'] = 'application/json';
      }
      headersSend.tsec = window.sessionStorage.getItem('tsec');
      Object.entries(headers).forEach(([key, value]) => {
        headersSend.append(key, value);
        headersSend[key] = key;
      });
      let options = {
        method: method,
        headers: headersSend,
        mode: 'cors'
      };
      if (body && method !== 'GET') {
        if (typeof body === 'object') {
          body = JSON.stringify(body);
        }
        options.body = body;
      }
      try {
        let requestFetch = new Request(url, options);
        let request = await this.executeReuest(requestFetch);
        let response;
        if (request.status >= 201 && request.status < 300) {
          response = {};
        } else {
          try {
            response = await request.json();
          } catch (e) {
            this.loading(false, noLoading);
            return { response: { status: 404, message: 'no found' }, request: { status: 404, statusText: 'no found' } };
          }
        }
        this.loading(false, noLoading);
        return { response, request };
      } catch (e) {
        this.sendError(e);
        this.loading(false, noLoading);
        return { response: { status: 404, message: 'no found' }, request: { status: 404, statusText: 'no found' } };
      }
    }
    console.log('Url no informada.');
    this.loading(false, noLoading);
    return { response: {}, request: {} };
  }

  sendError(error) {
    console.log('sendError', error);
  }

}

const sendRequest = new SendRequest();
export { sendRequest };